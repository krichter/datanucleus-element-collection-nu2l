package richtercloud.datanucleus.element.collection.nu2l;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author richter
 */
@Entity
public class MyEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    @ElementCollection(fetch = FetchType.EAGER)
    private Set<String> nameSynonyms = new HashSet<>();

    public MyEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<String> getNameSynonyms() {
        return nameSynonyms;
    }

    public void setNameSynonyms(Set<String> nameSynonyms) {
        this.nameSynonyms = nameSynonyms;
    }
}
